package MaximumSubArray;

import java.util.Arrays;

public class Padder {

    private int length;
    private char paddingChar;

    public Padder(int length, char paddingChar) {
        this.length = length;
        this.paddingChar = paddingChar;
    }

    public String pad(String input) {
        String paddedString;
        int lengthOfInput = input.length();
        int numberOfChar = length - lengthOfInput;
        StringBuilder pad = new StringBuilder(" ");

        if(numberOfChar < 0) {
            return "Input is too long.";
        }

        for(int i = 0; i < numberOfChar; i++) {
            pad.append(paddingChar);
        }

        paddedString = pad + input;

        return paddedString;
    }

    public String printEin(int[] intArr, int i, int n){

        int[] array = Arrays.copyOfRange( intArr, i-n+1, i+1);

        String stringedArray = Arrays.toString(array);

        return this.pad(stringedArray);

    }

}
