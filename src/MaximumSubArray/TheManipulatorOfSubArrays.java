package MaximumSubArray;

import java.util.Arrays;
import java.util.stream.IntStream;

public class TheManipulatorOfSubArrays {

    public int EinSum(int[] intArr, int i, int n) {

        int[] array = Arrays.copyOfRange(intArr, i - n + 1, i + 1);
        return IntStream.of(array).sum();

    }

    public int MaxSubArrayBrute(int[] intArr) {
        int sum;
        int[] maxArray = new int[intArr.length];
        int max = Integer.MIN_VALUE;
        for (int i = 0; i < intArr.length; i++) {
            sum = 0;
            System.out.println("---------------------------------------------------\n");
            System.out.println(i);
            for (int j = i; j < intArr.length; j++) {
                System.out.print(Arrays.toString(Arrays.copyOfRange(intArr, i, j + 1)));
                sum = sum + intArr[j];
                System.out.printf(" sum: %d", sum);
                System.out.printf(" max: %d\n", max);
                if (sum > max) {
                    max = sum;
                    maxArray = Arrays.copyOfRange(intArr, i, j + 1);
                }
            }
        }
        System.out.println("The maximum of the subarrays is " + max);
        System.out.println("The subarray having the maximum sum: " + Arrays.toString(maxArray));
        return max;
    }


    public int maxSumKadane(int[] intArr) {
        int tempSum = intArr[0];
        int maxSum = tempSum;
        int[] maxSumArray = new int[intArr.length];
        maxSumArray = Arrays.copyOfRange(intArr, 0,1);

        for (int i = 1; i < intArr.length; i++) {
                if(intArr[i] + tempSum > intArr[i]){
                    tempSum = intArr[i] + tempSum;
                    maxSumArray = Arrays.copyOfRange(intArr, i-maxSumArray.length, i+1);
                }else{
                    tempSum = intArr[i];
                    maxSumArray = Arrays.copyOfRange(intArr, i, i+1);
                }
            }
            if(tempSum > maxSum){
                maxSum = tempSum;
            }

        System.out.println(Arrays.toString(maxSumArray));
        return maxSum;
    }
}
